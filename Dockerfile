ARG UBUNTU_VERSION
FROM ubuntu:${UBUNTU_VERSION}
LABEL maintainer="Daniel von Essen"

ENV DEBIAN_FRONTEND=noninteractive

# Install dependencies.
RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install -y --no-install-recommends sudo systemd python3 \
  && apt-get clean

# Create `ansible` user with sudo permissions
RUN groupadd -r ansible \
  && useradd -m -g ansible ansible \
  && echo "%ansible ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/ansible
